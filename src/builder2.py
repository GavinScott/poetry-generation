import sqlite3
import sys
import random
import form_chooser as fc
import os

MAX_SPLITS = 3
MAX_ATTEMPTS = 5
CACHE_DIR = 'generated_poems/'

FORMS = {
    'haiku' : [{}, [
        {
            'lines': 3,
            'rym': 'abc',
            'syllables': (5, 7, 5),
            'pos': [
                ['VB', 'AT', 'JJ', 'NN'],
                ['VB', 'AT', 'JJ', 'JJ'],
                ['VB', 'AT', 'JJ', 'NN']
            ]
        }
    ]],

    'sonnet' : [{}, [
        {
            'lines': 14,
            'rym': 'ababcdcdefefgg',
            'syllables': (10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10),
            'pos': [
                ['VB', 'AT', 'JJ', 'NN'],
                ['VB', 'AT', 'JJ', 'JJ'],
                ['VB', 'AT', 'JJ', 'JJ'],
                ['VB', 'AT', 'JJ', 'NN'],
                
                ['VB', 'AT', 'JJ', 'JJ'],
                ['VB', 'AT', 'JJ', 'NN'],
                ['VB', 'AT', 'JJ', 'NN'],
                ['VB', 'AT', 'JJ', 'JJ'],
                
                ['VB', 'AT', 'JJ', 'JJ'],
                ['VB', 'AT', 'JJ', 'NN'],
                ['VB', 'AT', 'JJ', 'NN'],
                ['VB', 'AT', 'JJ', 'NN'],
                
                ['VB', 'AT', 'JJ', 'JJ'],
                ['VB', 'AT', 'JJ', 'NN']
            ]
        }
    ]],
    
    'limerick' : [{}, [
        {
            'lines': 5,
            'rym': 'aabba',
            'syllables': (8, 8, 5, 5, 8),
            'pos': [
                ['VB', 'AT', 'JJ', 'NN'],
                ['VB', 'AT', 'JJ', 'NN'],
                ['VB', 'AT', 'JJ', 'JJ'],
                ['VB', 'AT', 'JJ', 'JJ'],
                ['VB', 'AT', 'JJ', 'NN']
            ]
        }
    ]]
}

def get_db_name():
    return [a for a in sys.argv if '.db' in a][0]

def do_query(query, db_name=None):
    if db_name == None:
        db_name = get_db_name()
    conn = sqlite3.connect(db_name)
    cursor = conn.execute(query)
    ret = [c for c in cursor]
    conn.close()
    return ret

def merge_dicts(dicts):
    if len(dicts) == 1:
        return dicts[0]
    s = get_string(dicts[0])
    
    for d in dicts:
        print('\n\nMERGING:', d)
        print('\t', get_string(d))
    
    return_dict = {}
    # first
    return_dict['firstWord'] = s
    return_dict['secondWord'] = 'None'
    return_dict['secondLastWord'] = 'None'
    return_dict['middleWord'] = 'None'
    
    # last
    all_words = []
    for d in dicts[1:]:
        all_words.append(get_string(d, True))
    return_dict['lastWord'] = ' '.join(all_words)
    return_dict['rhyme'] = dicts[-1]['rhyme']
    
    print('\n\nRET:', return_dict)
    print('\t', get_string(return_dict))
    
    return return_dict
    

def to_dict(res):
    keys = ('firstWord',
            'secondWord',
            'middleWord',
            'secondLastWord',
            'lastWord',
            'firstPOS',
            'secondPOS',
            'middlePOS',
            'secondLastPOS',
            'lastPOS',
            'rhyme',
            'phonemes',
            'syllables',
            'firstSyllables',
            'n')
    dicts = []
    for n in range(0, len(res), len(keys)):
        d = {}
        for i in range(len(keys)):
            d[keys[i]] = res[n + i]
        dicts.append(d)
    return merge_dicts(dicts)

def handle_pos_condition(pos, n):
    where = []
    # first word
    where.append('firstPOS="' + pos[0] + '"')
    # last word
    if len(pos) > 1:
        where.append('lastPOS="' + pos[-1] + '"')
    # second/second-last word
    if len(pos) > 2 and n > 2:
        where.append('secondPOS="' + pos[1] + '"')
        where.append('secondLastPOS="' + pos[-2] + '"')
    # middle words
    if len(pos) > 4 and n > 4:
        where.append('middlePOS="' + '|'.join(pos[2:-2]) + '"')
    return where

def get_string(ngram, skip_first=False):
    if skip_first:
        s = []
    else:
        s = [ngram['firstWord']]
    if ngram['secondWord'] != 'None':
        s.append(ngram['secondWord'])
    if ngram['middleWord'] != 'None':
        s.append(ngram['middleWord'])
    if ngram['secondLastWord'] != 'None' and ngram['secondLastWord'] != ngram['secondWord']:
        s.append(ngram['secondLastWord'])
    s.append(ngram['lastWord'])
    return ' '.join(s)


def gen_line(splits, pos, syllables, rhyme=None, rhyme_not=None, \
        forbidden_ends=None, db_name=None):
    print('\n\nSplitting', splits, 'times...')
    if splits == MAX_SPLITS:
        print('\tToo many splits!')
        return None, None
    query = 'SELECT *\nFROM '
    # from clause
    from_parts = []
    for i in range(splits + 1):
        from_parts.append('p' + str(i))
    query += 'poems ' + ', poems '.join(from_parts) + '\n'
    # WHERE CLAUSE
    where = []
    # link parts
    for i in range(splits):
        where.append('p'+str(i)+'.lastPOS=' + 'p'+str(i+1)+'.firstPOS')
    # syllable count
    syl_str = []
    syl_str.append('p0.syllables')
    for i in range(1, splits + 1):
        syl_str.append('+ p'+str(i) + '.syllables')
        syl_str.append('- p'+str(i) + '.firstSyllables')
    where.append('(' + ' '.join(syl_str) + ') = ' + str(syllables))
    # rhyme & not-rhyme
    if rhyme != None:
        where.append('p'+str(splits) + '.rhyme = "' + rhyme + '"')
    if rhyme_not != None and len(rhyme_not) > 0:
        nots = ['"' + n + '"' for n in rhyme_not]
        for n in nots:
            where.append('p'+str(splits) + '.rhyme != ' + n)
    # forbidden last words
    if forbidden_ends != None:
        forbids = ['"' + f + '"' for f in forbidden_ends]
        for f in forbids:
            where.append('p'+str(splits) + '.lastWord != ' + f)
    # last word POS
    where.append('p'+str(splits) + '.lastPOS = "' + pos[-1] + '"')
    # finish query
    query += 'WHERE ' + '\n\tAND '.join(where)
    
    print('*************************************')
    print(query)
    print('*************************************')
    
    # run query
    results = do_query(query, db_name)
    if len(results) > 0:
        # pick a result
        chosen = to_dict(random.choice(results))
        return get_string(chosen), chosen['rhyme']
    else:
        # no results, retry with more splits
        print('\tNo results, retrying with another split...')
        return gen_line(splits + 1, pos, syllables, rhyme, rhyme_not, forbidden_ends, db_name)


def gen_poem(form, db_name=None):
    stanzas = []
    lines = []
    s_n = 2
    # generate stanzas
    for stanza in form[1]:
        rhymes = {}
        # generate lines
        for line_num in range(stanza['lines']):
            print('--------------------------------------------------- Getting line', line_num + 1)
            # line requirements
            rym = stanza['rym'][line_num]
            pos = [p for p in stanza['pos'][line_num] if '<' not in p]
            print('POS:', pos)
            syllables = stanza['syllables'][line_num]
            if syllables > 0:
                # rhyme scheme for new rhymes
                rhyme = None
                rhyme_not = list(rhymes.values())
                # taken ends
                forbidden_ends = []
                if len(lines) > 0:
                    forbidden_ends = [l.split()[-1].lower() for l in lines if len(l) > 0]
                    forbidden_ends.extend([l.split()[-1].title() for l in lines])
                # rhyme scheme for previously seen rhymes
                if rym in rhymes:
                    rhyme = rhymes[rym]
                    rhyme_not = None
                
                # generate line
                line, end_rhyme = gen_line(0, pos, syllables, rhyme, rhyme_not, \
                    forbidden_ends, db_name)
                if line == None:
                    # ERROR
                    return None
                # pretty-up line
                line = line.replace('|', ' ')
                line = ''.join([c for c in line if c.isalpha() or c == ' '])
                line = line.replace('  ', ' ')
                lines.append(line.strip())
                # save rhyme
                rhymes[rym] = end_rhyme
            else:
                lines.append('')
        # print poem
        print('\n\nRhyme scheme:', stanza['rym'])
        print('\n\nStanza:')
        for l in lines:
            print('\t', l)
        stanzas.append(lines)
    return stanzas

def cache_poem(folder, poem):
    if len(os.listdir(CACHE_DIR + folder)) == 0:
        n = 0
    else:
        n = max([int(x.replace('.json', '').replace('.poem', '')) \
            for x in os.listdir(CACHE_DIR + folder)]) + 1
    print('Saving to ' + str(n) + '.poem')
    with open(CACHE_DIR + folder + '/' + str(n)  + '.poem', 'w+') as f:
        s = ''
        for stanza in poem:
            for line in stanza:
                s += line + '\n'
            s += '\n'
        f.write(s.strip())

def build_poem_from_cache_and_db(folder, db_name=None):
    print('----------------------')
    print('CHOOSING FORM...')
    print('----------------------')
    poem_dir = 'poems/' + folder
    form = None
    
    print('\n----------------------')
    print('GENERATING CONTENT...')
    print('----------------------')
    poem = None
    attempts = 1
    while poem == None:
        print('******\n\n\nATTEMPT:', attempts, '\n\n\n******')
        attempts += 1
        if attempts == MAX_ATTEMPTS or form == None:
            attempts = 1
            if os.path.isdir(folder):
                form = fc.chooseForm(folder)
            else:
                # hard-coded forms
                print('Getting hardcoded form')
                form = FORMS[folder]
        poem = gen_poem(form, db_name)
    print('\n\n\nFORM:', form)
    print('\nPOEM:')
    for s in poem:
        print('\t' + '\n\t'.join(s))
        print()
    cache_poem(folder, poem)
    return poem
    
if __name__ == '__main__':
    folder = sys.argv[1]
    poem = build_poem_from_cache_and_db(folder)
            
            
            
            
            
            
