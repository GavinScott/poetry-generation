import os
import features
import sys

# gets a list of features for every poem in a directory
def read(folder):
    data = []
    print('READING FROM:', folder)
    #for fname in [p for p in os.listdir(folder) if '.poem' in p]:
    for fname in [p for p in os.listdir(folder) if '.json' == p[-5:]]:
        data.append(features.extract(folder + fname.replace('.json', '.poem')))
        #print('\tadded:', folder + fname)
    print('loaded', len(data), 'poem features')
    return data
    
if __name__ == '__main__':
    folder = sys.argv[1]
    read(folder)
