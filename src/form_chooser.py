import reader
import sys
import features
import random

def chooseForm(folder):
    data = reader.read(folder)
    glob_features = {}
    loc_features = {}
    
    NUM_STANZAS = 'num_stanzas'
    
    # init
    for gkey in features.GLOBAL_KEYS:
        glob_features[gkey] = []
    glob_features[NUM_STANZAS] = []
    for key in features.KEYS:
        loc_features[key] = []
    
    # extract all features
    for poem in data:
        gf = poem[0]
        stanzas = poem[1]
        for gkey in features.GLOBAL_KEYS:
            glob_features[gkey].append(gf[gkey])
        glob_features[NUM_STANZAS].append(len(stanzas))
        for line in stanzas:
            for key in features.KEYS:
                loc_features[key].append(line[key])
    
    # choose form
    chosen_global = {}
    for gkey in features.GLOBAL_KEYS:
        chosen_global[gkey] = random.choice(glob_features[gkey])
    # local features per stanza
    numStanzas = random.choice(glob_features[NUM_STANZAS])
    print('stanzas:', numStanzas)
    stanzas = []
    for i in range(0, numStanzas):
        stanza = {}
        stanza[features.LINES_KEY] = random.choice(loc_features[features.LINES_KEY])
        for key in [fe for fe in features.KEYS if fe != features.LINES_KEY]:
            val = []
            while len(val) < stanza[features.LINES_KEY]:
                val = random.choice(loc_features[key])
            stanza[key] = val[:stanza[features.LINES_KEY]]
        stanzas.append(stanza)
    
    # build final form
    form = (chosen_global, stanzas)
    return form
    


if __name__ == '__main__':
    folder = sys.argv[1]
    form = chooseForm(folder)
    features.printForm(form)
