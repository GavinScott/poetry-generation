POEM_DIR = 'poems/'
poem_url = 'http://www.poetrysoup.com/'

from bs4 import BeautifulSoup as bs
import urllib.request as urllib
import os

def hasAlpha(line):
    for c in line:
        if c.isalpha():
            return True
    return False

# Run this to scrape a new type of poem
def scrape(url, forceLen=None):
    global poem_url
    poem_type = url.split('/')[-1] + 's'
    d = POEM_DIR + poem_type + '/'
    if not os.path.exists(d):
        os.makedirs(d)
    soup = bs(urllib.urlopen(url).read(), 'lxml')
    poems = soup.find_all("a", {"class": "breakword"})
    results = []
    
    for poem in poems:
        fname = d + poem['title'].replace(' ', '_')+'.poem'
        if not os.path.exists(fname):
            FAIL = False
            p_url = poem_url + poem['href'][3:]
            p_soup = str(bs(urllib.urlopen(p_url).read(), 'html.parser'))
            if '<pre' in p_soup:
                pre = p_soup.split('<pre')[1]
                pre = pre.split('/pre')[0]
                pre = pre.replace('<b>', '')
                pre = pre.replace('</b', '')
                pre = pre.split('>')
                for ndx in range(0, len(pre)):
                    if '\r\n' in pre[ndx]:
                        break
                
                text = [x.strip() for x in pre[ndx].strip().split('\r\n') if len(x.strip()) > 0]
                for toDel in ['<em>', '</em>', '</em', '<b>', '</b>', '<', '~']:
                    text = [x.replace(toDel, '') for x in text]
                text = [x for x in text if hasAlpha(x)]
                if forceLen != None:
                    text = text[:forceLen]
                    if len(text) != forceLen:
                        FAIL = True
                        print('ERROR: wrong length:', len(text), '| Title:', '"' + poem['title'] + '"')
                
                if not FAIL:
                    print(poem['title'])
                    print(text)
                    print()
                    results.append(text)
                    with open(fname, 'w+') as f:
                        for line in text:
                            f.write(line + '\n')
                else:
                    print('"' + poem['title'] + '"', 'not saved.\n')
        else:
            print(poem['title'], 'already saved (skipping)')
                
    return results





# Add new poem types calls here
#scrape('http://www.poetrysoup.com/poems/haiku', 3)
#scrape('http://www.poetrysoup.com/poems/sonnet', 14)
scrape('http://www.poetrysoup.com/poems/Cinquain', 5)
scrape('http://www.poetrysoup.com/poems/couplet', 2)
#scrape('http://www.poetrysoup.com/poems/italian_sonnet', 14)
#scrape('http://www.poetrysoup.com/poems/Limerick', 5)
scrape('http://www.poetrysoup.com/poems/Quatrain', 4)
scrape('http://www.poetrysoup.com/poems/Rondeau')

stats = {}
for d in os.listdir('./poems'):
    if d[0] != '.':
        stats[d.title()] = len(os.listdir('./poems/' + d))
print('\n\nSTATS:')
for d in stats:
    print('\t', d, ':', stats[d])
print()

