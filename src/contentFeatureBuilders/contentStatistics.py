#! /usr/bin/env python3

import sys
import os
from argparse import ArgumentParser
import json

from featureBuilder import *

from nltk.corpus import cmudict

def main():
    parser = ArgumentParser(description="Prints out statistics for each of the corpuses", prog = sys.argv[0])
    parser.add_argument("-b", "--brown",
            action = "store_true",
            default = False,
            help="outputs statistics for the brown corpus")
    args = parser.parse_args()
    cmu = cmudict.dict()
    print("CMUDict Words: %d" % len(cmu))
    print(", ".join(sorted(SyllableCounter.ACCEPTED_SYLLABLES)))
    return 0

if __name__ == "__main__":
    rtn = main()
    sys.exit(rtn)
