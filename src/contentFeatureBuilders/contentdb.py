#! /usr/bin/env python3

import sys
import os

import sqlite3
from featureBuilder import *

from argparse import ArgumentParser

class ContentDB(object):
    features = [
                "firstWord",
                "secondWord",
                "middleWord",
                "secondLastWord",
                "lastWord",
                "firstPOS",
                "secondPOS",
                "middlePOS",
                "secondLastPOS",
                "lastPOS",
                "rhyme",
                "phonemes",
                "syllables",
                "firstSyllables",
                "n"
            ]
    # With Syntax
    def __enter__(self):
        return self
    def __exit__(self, exc_type, exc_value, traceback):
        self.close()
        return
    def __init__(self, name):
        self.inserts = 0
        self.duplicates = 0
        self.conn = sqlite3.connect(name)
        try:
            self.conn.execute("""CREATE TABLE poems (
                firstWord VARCHAR(50),
                secondWord VARCHAR(50),
                middleWord VARCHAR(200),
                secondLastWord VARCHAR(50),
                lastWord VARCHAR(50),
                firstPOS VARCHAR(20),
                secondPOS VARCHAR(20),
                middlePOS VARCHAR(20),
                secondLastPOS VARCHAR(20),
                lastPOS VARCHAR(20),
                rhyme VARCHAR(25),
                phonemes VARCHAR(50),
                syllables INT,
                firstSyllables INT,
                n INT,
                PRIMARY KEY (
                firstWord, secondWord, middleWord, secondLastWord, lastWord,
                firstPOS, secondPOS, middlePOS, secondLastPOS, lastPOS, phonemes, n, syllables)
            );""")
        except Exception as e:
            print(e)
        return
    def insertNGramFeatures(self, ngramFeatures):
        cols = ",".join([f for f in ContentDB.features])
        vals = ",".join(['"%s"' % ngramFeatures.__dict__[f] for f in ContentDB.features])
        statement = 'INSERT INTO poems (' + cols + ') VALUES (' + vals + ')'
        try:
            self.conn.execute(statement)
            self.inserts += 1
        except Exception as e:
            # Consider updating a counter here in the database
            self.duplicates += 1
        return
    def commit(self):
        print("Committing")
        self.conn.commit()
    def close(self):
        print("Closing connection")
        self.conn.close()

def main():
    parser = ArgumentParser(description = "Builds a Brown corpus ngram content database.", prog = sys.argv[0])
    parser.add_argument("filename",
            metavar = "database.db",
            help="file to save the database to")
    parser.add_argument("-i", "--insult", 
            nargs = 1,
            default = None,
            help = "grabs ngram features for the supplied insult text")
    parser.add_argument("-c", "--curse", 
            nargs = 1,
            default = None,
            help = "grabs ngram features for the supplied curse text")
    parser.add_argument("-t", "--trump", 
            nargs = 1,
            default = None,
            help = "grabs ngram features for the supplied trump text")
    parser.add_argument("-b", "--brown", 
            action = "store_true",
            default = False,
            help = "grabs ngram features for brown corpus")
    args = parser.parse_args()
    content = ContentDB(args.filename)
    allNGramFeatures = []
    if args.brown:
        allNGramFeatures = extractBrownFeatures(2,5, None, 1)
    if args.trump:
        words = getTrumpWords(args.trump[0])
        allNGramFeatures = extractNGramFeatures(words, 2, 6, 1)
    if args.insult:
        words = getInsultWords(args.insult[0])
        allNGramFeatures = extractNGramFeatures(words, 2, 7, None)
    if args.curse:
        words = getCurseWords(args.curse[0])
        allNGramFeatures = extractNGramFeatures(words, 2, 6, None)
    print("Inserting %d into database" % len(allNGramFeatures))
    placed = 0
    for ngramFeatures in allNGramFeatures:
        content.insertNGramFeatures(ngramFeatures)
        if placed % 100000 == 0:
            print("total %d:inserts %d:duplicates %d" %( placed, content.inserts, content.duplicates))
            content.commit()
        placed += 1
    print("%d inserts" % content.inserts)
    print("%d duplicates" % content.duplicates)
    content.commit()
    
    cursor = content.conn.execute('SELECT * FROM poems')
    if len([r for r in cursor]) < 15:
        for row in cursor:
           print(row)
    
    content.close()

    return

if __name__ == "__main__":
    rtn = main()
    sys.exit(rtn)
