#! /usr/bin/env python3

import sys
import os
import itertools
from argparse import ArgumentParser

from syllables import *

import sqlite3
from pprint import pprint

from nltk.corpus import brown
from nltk import ngrams
from nltk import everygrams

class NGramFeatures(object):
    delimiter = "|"
    counter = SyllableCounter()
    def saveSyllables():
        if NGramFeatures.counter:
            NGramFeatures.counter.save()
    # Splitter
    def splitNGram(ngram):
        assert len(ngram) >= 2,\
                "NGram must be of length >= 2. Received: %d" % len(ngram)
        first = ngram[0]
        second, middle, secondLast = (None, None, None)
        last = ngram[-1]
        if len(ngram) > 2:
            second = ngram[1]
            secondLast = ngram[-2]
        if len(ngram) > 4:
                middle = NGramFeatures.delimiter.join(ngram[2:-2])
        return first, second, middle, secondLast, last
    def getRhymingPart(phonemes):
        # Emulated from: https://github.com/aparrish/pronouncingpy/blob/\
        #     87b705339ab2834e1f691ad86da08f3f91d47b57/pronouncing/__init__.py#L124
        phonemes = phonemes.split(NGramFeatures.delimiter)
        for i in range(len(phonemes) - 1, 0, -1):
            if phonemes[i][-1] in "12":
                return NGramFeatures.delimiter.join(phonemes[i:])
        return phonemes
    def countSyllables(ngram):
        syllables = 0
        counter = NGramFeatures.counter
        for word in ngram:
            syllables += counter.countSyllables(word)
        return syllables
    def getPossiblePronunciationsAndSyllables(ngram, limit = None):
        counter = NGramFeatures.counter
        allPronsAndSylls = [counter.getPronunciationsAndSyllables(word) for word in ngram]
        transpose = []
        for b in allPronsAndSylls:
            wordPronsAndSylls = []
            for pron, syll in zip(b[0], b[1]):
                wordPronsAndSylls.append((pron, syll))
            transpose.append(wordPronsAndSylls)
        return [element for element in itertools.product(*transpose)]
    def getFullPhonemesAndSyllables(ngram, limit = None):
        phonemesAndSyllables = []
        allPossible = NGramFeatures.getPossiblePronunciationsAndSyllables(ngram, limit)
        for whole in allPossible:
            allPhonemes = []
            totalSyllables = 0
            for pron, syll in whole:
                allPhonemes.extend(pron)
                totalSyllables += syll
            phonemesAndSyllables.append((NGramFeatures.delimiter.join(allPhonemes), totalSyllables))
            if limit and len(phonemesAndSyllables) >= limit:
                break
        #assert len(phonemesAndSyllables), "%s has no pronunciation" % (ngram,)
        return phonemesAndSyllables


    # Constructor
    def __init__(self, ngram, ngramPOS, phonemes, syllables):

        assert len(ngram) == len(ngramPOS),\
            "Need same length word(%d) and POS(%d) ngram" % (len(ngram), len(ngramPOS))

        # Save Canonical Representations
        self.ngram = ngram
        self.ngramPOS = ngramPOS
        self.n = len(ngram)
        self.phonemes = phonemes
        self.rhyme = NGramFeatures.getRhymingPart(phonemes)
        self.syllables = syllables
        self.firstSyllables = NGramFeatures.counter.countSyllables(ngram[0])[0]

        # Split NGram
        self.firstWord, self.secondWord, self.middleWord, self.secondLastWord,\
            self.lastWord = NGramFeatures.splitNGram(ngram)

        # Split POS NGram
        self.firstPOS, self.secondPOS, self.middlePOS, self.secondLastPOS,\
            self.lastPOS = NGramFeatures.splitNGram(ngramPOS)
        return

    # Hashing
    def __eq__(self, other):
        return self.ngram == other.ngram and self.ngramPOS and other.ngramPOS
    def __hash__(self):
        return hash((self.ngram, self.ngramPOS, self.rhyme, self.syllables))
    # Printing
    def __str__(self):
        return "%s:%s:%s" % ([(word, pos) for (word, pos) in zip(self.ngram, self.ngramPOS)],self.syllables,self.phonemes)


from nltk.tokenize import word_tokenize
from nltk import pos_tag
def filterWords(words):
    print("Filtering Words")
    filteredWords = []
    for word in words:
        if word == ",":
            continue
        if word == "-":
            continue
        if word == "``":
            continue
        if word == "''":
            continue
        if word == "\"":
            continue
        filteredWords.append(word)
    return filteredWords
def getTrumpWords(filename):
    texts = None
    with open(filename) as file:
        texts = json.load(file)

    print("Tokenizing Trump")
    allText = " ".join([m for m in map(lambda x : x["text"], texts)])
    words = filterWords(word_tokenize(allText))

    return words
def getInsultWords(filename):
    submissions = None
    with open(filename) as file:
        submissions = json.load(file)

    print("Tokenizing Insult")
    allText = " ".join([m for m in map(lambda x : "%s %s" % (x["title"],x["selftext"]), submissions)])
    words = filterWords(word_tokenize(allText))
    return words
def getCurseWords(filename):
    submissions = None
    with open(filename) as file:
        submissions = json.load(file)

    print("Tokenizing Curses")
    allText = " ".join([m for m in map(lambda x : "%s" % (x["title"] + ".",), submissions)])
    words = filterWords(word_tokenize(allText))
    return words
def extractNGramFeatures(words, low, high, phonemesLimit = None):
    print("Tagging")
    allPOS = pos_tag(words)
    wordsPOS = []
    for word,pos in zip(words, allPOS):
        wordsPOS.append((word.lower(),pos))

    print("Building N-Grams")
    ngrams = everygrams(wordsPOS, low, high)
    ngramFeatures = []

    print("Making Features")
    preview = 0
    numPossible = 0
    numNGrams = 0
    for ngram in ngrams:
        ngramWord = tuple([word for (_, (word, pos)) in ngram])
        ngramPOS = tuple([pos for (_, (word, pos)) in ngram])
        for phonemes, syllables in NGramFeatures.getFullPhonemesAndSyllables(ngramWord, limit = phonemesLimit):
            features = NGramFeatures(ngramWord, ngramPOS, phonemes, syllables)
            ngramFeatures.append(features)
            if numPossible % 100000 == 0:
                print("Possible:%d" % (numPossible))
            numPossible += 1
        if numNGrams % 100000 == 0:
            print("NGrams:%d" % (numNGrams))
        numNGrams += 1
    return ngramFeatures

def extractBrownFeatures(low, high, brownLimit=None, phonemesLimit = None):
    wordPOS = []

    print("Getting tagged words")
    for word, pos in brown.tagged_words()[:brownLimit]:
        wordPOS.append((word.lower(), pos))

    print("Building everygrams")
    ngrams = everygrams(wordPOS, low, high)
    ngramFeatures = []

    print("Making features")
    numPossible = 0
    numNGrams = 0
    for ngram in ngrams:
        ngramWord = tuple([word for (word, pos) in ngram])
        ngramPOS = tuple([pos for (word, pos) in ngram])
        for phonemes, syllables in NGramFeatures.getFullPhonemesAndSyllables(ngramWord, limit = phonemesLimit):
            ngramFeatures.append(NGramFeatures(ngramWord, ngramPOS, phonemes, syllables))
            if numPossible % 100000 == 0:
                print("Possible:%d" % (numPossible))
            numPossible += 1
        if numNGrams % 100000 == 0:
            print("NGrams:%d" % (numNGrams))
        numNGrams += 1
    print("Done")
    return ngramFeatures

def printExample(ngram, ngramPOS):
    print(len(ngram))
    for phonemes,syllables in NGramFeatures.getFullPhonemesAndSyllables(ngram):
        features = NGramFeatures(ngram, ngramPOS, phonemes, syllables)
        print("Word NGram      :", ngram)
        print("Word Rhyme      :", features.rhyme)
        print("Word Syllables  :", features.syllables)
        print("Word First      :", features.firstWord)
        print("Word Second     :", features.secondWord)
        print("Word Middle     :", features.middleWord)
        print("Word Second Last:", features.secondLastWord)
        print("Word Last       :", features.lastWord)
        print("POS  NGram      :", ngram)
        print("POS  First      :", features.firstPOS)
        print("POS  Second     :", features.secondPOS)
        print("POS  Middle     :", features.middlePOS)
        print("POS  Second Last:", features.secondLastPOS)
        print("POS  Last       :", features.lastPOS)
        print(features)
    return

def main():
    parser = ArgumentParser(
            description = "Builds the ngram feature set to insert into a database", 
            prog = sys.argv[0])
    parser.add_argument("-i", "--insult", 
            nargs = 1,
            default = None,
            help = "grabs ngram features for the supplied insult text")
    parser.add_argument("-c", "--curse", 
            nargs = 1,
            default = None,
            help = "grabs ngram features for the supplied curse text")
    parser.add_argument("-t", "--trump", 
            nargs = 1,
            default = None,
            help = "grabs ngram features for the supplied trump text")
    parser.add_argument("-b", "--brown", 
            action = "store_true",
            default = False,
            help = "grabs ngram features for brown corpus")
    parser.add_argument("-e", "--example", 
            action = "store_true",
            default = False,
            help = "prints out an example of how the ngrams are split up")
    parser.add_argument("-d", "--different", 
            default = None,
            nargs = "*",
            help = "prints out an example of how the ngrams are split up")
    args = parser.parse_args()
    if args.example:
        ngrams = [
                ("banana", "pancake"),
                ("eat", "banana", "pancake"),
                ("eat", "a", "banana", "pancake"),
                ("he", "eats", "a", "banana", "pancake"),
                ("he", "eats", "all", "the", "banana", "pancakes")
                ]
        ngramsPOS = [
                ("noun", "noun"),
                ("verb", "noun", "noun"),
                ("verb", "uh", "noun", "noun"),
                ("pronoun", "verb", "uh", "noun", "noun"),
                ("pronoun", "verb", "uh", "article", "noun", "noun")
                ]
        for ngram, ngramPOS in zip(ngrams, ngramsPOS):
            printExample(ngram, ngramPOS)
    if args.different:
        ngram = tuple(args.different)
        counter = SyllableCounter()
        print(ngram)
        bleh = [counter.getPronunciationsAndSyllables(word) for word in ngram]
        newBleh = []
        for b in bleh:
            wordBleh = []
            for pron, syll in zip(b[0], b[1]):
                wordBleh.append((pron, syll))
                print(pron,syll)
            newBleh.append(wordBleh)
            print("fuck")
        for element in itertools.product(*newBleh):
            print(element)
        for phonemes, syllables in NGramFeatures.getFullPhonemesAndSyllables(ngram):
            print( "%s:%s:%s:%s" % (ngram, syllables, phonemes, NGramFeatures.getRhymingPart(phonemes)))
    if args.brown:
        extractBrownFeatures(2,5)
    if args.trump:
        words = getTrumpWords(args.trump[0])
        extractNGramFeatures(words, 2, 6, 1)
    if args.insult:
        words = getInsultWords(args.insult[0])
        extractNGramFeatures(words, 2, 6, 1)
    if args.curse:
        words = getCurseWords(args.curse[0])
        extractNGramFeatures(words, 2, 7, None)

    NGramFeatures.saveSyllables()
    return 0

if __name__ == "__main__":
    rtn = main()
    sys.exit(rtn)
