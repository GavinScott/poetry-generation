import nltk
import sys
from nltk.corpus import cmudict
import json

from logios.syllables import *

RHYME_KEY = 'rym'
REP_KEY = 'rep'
GLOBAL_RHYME_KEY = 'g_rym'
GLOBAL_REP_KEY = 'g_rep'
POS_KEY = 'pos'
LINES_KEY = 'lines'
SYLLABLES_KEY = 'syllables'

KEYS = [
    RHYME_KEY, REP_KEY, REP_KEY, POS_KEY, LINES_KEY, SYLLABLES_KEY
]

GLOBAL_KEYS = [
    GLOBAL_RHYME_KEY, GLOBAL_REP_KEY
]


# colors for printing
class bcolors:
    GREEN = '\033[92m'
    BLUE = '\033[94m'
    ENDC = '\033[0m'
COLOR_FLAG = '-c'
CUR_COLOR = bcolors.GREEN

# colored printing for easier reading
def printC(text):
    if '-c' in sys.argv:
        global CUR_COLOR
        print(CUR_COLOR + ' '.join([str(x) for x in text]) + bcolors.ENDC)
        if CUR_COLOR == bcolors.GREEN:
            CUR_COLOR = bcolors.BLUE
        else:
            CUR_COLOR = bcolors.GREEN
    else:
        print(' '.join([str(x) for x in text]))

def getJsonFname(fname):
    return '.'.join(fname.split('.')[:-1]) + '.json'

from nltk.tokenize import word_tokenize
counter = SyllableCounter(jarFilename = "./logios/counter.jar", logiosFilename = "./logios/logios.json")
def getSyllables(stanza):
    syllablesPerLine = []
    for line in stanza:
        syllables = 0
        for word in word_tokenize(line):
            syllables += counter.countSyllables(word)[0]
        syllablesPerLine.append(syllables)
    return syllablesPerLine

# gets all features
def extract(fname):
    # look for saved features (for speed)
    features = loadFeatures(fname)
    # new poem
    if features == None:
        stanzas = getStanzas(fname)
        stanzaFeatures = []
        for stanza in stanzas:
            features = {}
            features[LINES_KEY] = len(stanza)
            features[REP_KEY] = getRepScheme(stanza)
            features[RHYME_KEY] = getRhymeScheme(stanza)
            features[POS_KEY] = getPOS(stanza)
            features[SYLLABLES_KEY] = getSyllables(stanza)
            stanzaFeatures.append(features)

        global_features = {}
        global_features[GLOBAL_REP_KEY] = getGlobalRepScheme(stanzas)
        global_features[GLOBAL_RHYME_KEY] = getGlobalRhymeScheme(stanzas)

        features = (global_features, stanzaFeatures)

        #printPoem(fname, stanzas, features)
        saveFeatures(fname, features)
    return features

# saves poem features to json
def saveFeatures(fname, features):
    new_f = getJsonFname(fname)
    data = {}
    data['global'] = features[0]
    data['stanza'] = features[1]
    with open(new_f, 'w+') as f:
        json.dump(data, f)

# loads poem features from json
def loadFeatures(fname):
    if '.json' not in fname:
        new_f = getJsonFname(fname)
    else:
        new_f = fname
    data = None
    features = None
    try:
        with open(new_f, 'r') as f:
            data = json.load(f)
        features = (data['global'], data['stanza'])
    except:
        print('Could not load:', new_f)
    return features

def combineStanzas(stanzas):
    return [line for stanza in stanzas for line in stanza]

def getGlobalRepScheme(stanzas):
    return getRepScheme(combineStanzas(stanzas))

def getRepScheme(stanza):
    # line repetition scheme
    repeats = {}
    curLet = 'A'
    rep_scheme = ''
    for line in stanza:
        if line not in repeats:
            repeats[line] = curLet
            curLet = chr(ord(curLet) + 1)
        rep_scheme += repeats[line]
    return rep_scheme

def stripEndingPunc(line):
    words = line.split()
    stripped = []
    puncs = [',.?![]{}()!@#$%^&*-_+=;:"~`|/<>' + "'"]
    for word in words:
        for p in puncs:
            word = word.rstrip(p)
            word = word.lstrip(p)
        stripped.append(word)
    return ' '.join(stripped)

def getStanzas(fname):
    stanzas = []
    with open(fname, 'r') as f:
        lines = [x.strip().lower() for x in f.readlines()]
        stanza = []
        for line in lines:
            if len(line) > 0:
                stanza.append(stripEndingPunc(line))
            else:
                if len(stanza) > 0:
                    stanzas.append(stanza)
                stanza = []
        if len(stanza) > 0:
            stanzas.append(stanza)
    return stanzas

def getGlobalRhymeScheme(stanzas):
    return getRhymeScheme(combineStanzas(stanzas))

def getRhymeScheme(stanza):
    # get last words
    lasts = []
    for line in stanza:
        if len(line.split()) > 0:
            lasts.append(line.split()[-1])
    # get last word pronunciations
    prons = {}
    for last in lasts:
        p = [pron for (word, pron) in cmudict.entries() if word == last]
        if len(p) > 0:
            sp = int(len(p[0]) / 2) + 1
            if sp > 2:
                sp = 2
            prons[last] = tuple([tuple(pronun[-sp:]) for pronun in p])
        else:
            prons[last] = ('?',)
        
    # get scheme
    scheme = ''
    curLet = 'a'
    repeats = {}
    for last in lasts:
        found = False
        for p in prons[last]:
            for r in repeats:
                if p == r:
                    found = True
                    scheme += repeats[r]
                    break
        if not found:
            for p in prons[last]:
                repeats[p] = curLet
            scheme += curLet
            curLet = chr(ord(curLet) + 1)
    
    return scheme

def getWords(line):
    # words = word_tokenize(line)
    words = line.split()
    return words

def getPOS(stanza):
    pos_lines = []
    start = True
    for line in stanza:
        words = getWords(line)
        pos = [p[1] for p in nltk.pos_tag(words)]
        pos_lines.append(pos)
    return pos_lines

def printPoemDetailed(fname, stanzas, features):
    globalFeatures = features[0]
    stanzaFeatures = features[1]
    print('******************************************************************')
    print('POEM:', fname)
    global_ndx = 0
    for i in range(len(stanzas)):
        print('--------------------------------------------------------------')
        print('STANZA #' + str(i + 1))
        for line in range(stanzaFeatures[i][LINES_KEY]):
            printC([
                '  ', line + 1,
                '|', RHYME_KEY + ':', stanzaFeatures[i][RHYME_KEY][line],
                '|', GLOBAL_RHYME_KEY + ':', globalFeatures[GLOBAL_RHYME_KEY][global_ndx],
                '|', REP_KEY + ':', stanzaFeatures[i][REP_KEY][line],
                '|', GLOBAL_REP_KEY + ':', globalFeatures[GLOBAL_REP_KEY][global_ndx],
                '|', '"' + stanzas[i][line] + '"',
                '|', '-'.join(stanzaFeatures[i][POS_KEY][line])
                ])
            global_ndx += 1
    print('******************************************************************')
    
# prints a poem from just the filename
def printPoem(fname):
    features = extract(poem)
    stanzas = getStanzas(poem)
    printPoemDetailed(poem, stanzas, features)

def printForm(form):
    print('-------------------------------------------------')
    print('GLOBAL:')
    for k in form[0]:
        print('\t', k + ':', form[0][k])
    print('LOCAL:')
    for i in range(len(form[1])):
        s = form[1][i]
        print('\tStanza:', i + 1)
        for k in s:
            if k != POS_KEY:
                print('\t\t', k + ':', s[k])
        print('\t\t', POS_KEY + ':')
        for l in s[POS_KEY]:
            print('\t\t\t', l)
    print('-------------------------------------------------')

if __name__ == '__main__':
    poem = 'test.poem'
    printPoem(poem)
    
    f = extract(poem)
    printForm(f)
    
    
    
    
    
    
    
    
    
