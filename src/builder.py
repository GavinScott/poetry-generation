import sqlite3
import sys
import random
import form_chooser as fc

def get_db_name():
    return [a for a in sys.argv if '.db' in a][0]

def do_query(query):
    db_name = get_db_name()
    conn = sqlite3.connect(db_name)
    cursor = conn.execute(query)
    ret = [c for c in cursor]
    conn.close()
    return ret

def to_dict(res):
    d = {}
    keys = ('firstWord',
            'secondWord',
            'middleWord',
            'secondLastWord',
            'lastWord',
            'firstPOS',
            'secondPOS',
            'middlePOS',
            'secondLastPOS',
            'lastPOS',
            'rhyme',
            'phonemes',
            'syllables',
            'n')
    for i in range(len(keys)):
        d[keys[i]] = res[i]
    return d

def handle_pos_condition(pos, n):
    where = []
    # first word
    where.append('firstPOS="' + pos[0] + '"')
    # last word
    if len(pos) > 1:
        where.append('lastPOS="' + pos[-1] + '"')
    # second/second-last word
    if len(pos) > 2 and n > 2:
        where.append('secondPOS="' + pos[1] + '"')
        where.append('secondLastPOS="' + pos[-2] + '"')
    # middle words
    if len(pos) > 4 and n > 4:
        where.append('middlePOS="' + '|'.join(pos[2:-2]) + '"')
    return where

def req_ngram(start_n=2, rhyme=None, rhyme_not=None, pos=None, cur_syllables=None, max_syllables=None):
    print('MAX SYLS:', max_syllables)
    if start_n == 0:
        print('ERROR: Impossible')
        exit(-1)
    query = 'SELECT * FROM poems WHERE '
    where = []
    # ngram size
    where.append('n=' + str(start_n))
    # rhyme
    if rhyme != None:
        where.append('rhyme="' + str(rhyme) + '"')
    # not rhyme
    if rhyme_not != None:
        for r in [r for r in rhyme_not if r != rhyme]:
            where.append('rhyme!="' + str(r) + '"')
    # pos
    if pos != None:
        where.extend(handle_pos_condition(pos, start_n))
    # syllables
    if max_syllables != None:
        where.append('syllables<=' + str(max_syllables - cur_syllables))
    
    # get results
    query = query + ' AND '.join(where)
    print('\n*****\n', query, '\n*****\n')
    results = do_query(query)
    if len(results) == 0:
        # backoff restrictions
        return req_ngram(start_n=(start_n - 1), rhyme=rhyme, \
            rhyme_not=rhyme_not, pos=pos, cur_syllables=cur_syllables, \
            max_syllables=max_syllables)
    chosen = to_dict(random.choice(results))
    return chosen

def get_string(ngram, skip_first=False):
    if skip_first:
        s = []
    else:
        s = [ngram['firstWord']]
    if ngram['secondWord'] != 'None':
        s.append(ngram['secondWord'])
    if ngram['middleWord'] != 'None':
        s.append(ngram['middleWord'])
    if ngram['secondLastWord'] != 'None':
        s.append(ngram['secondLastWord'])
    s.append(ngram['lastWord'])
    return ' '.join(s)
    
    
def gen_line(pos, rhyme=None, rhyme_not=None, syllables=None):
    s_n = 2
    parts = []
    end_rhyme = None
    syllable_count = 0
    for i in range(len(pos) - 1):
        tup = pos[i:i+s_n]
        if i == len(pos) - 2:
            print('hit last ngram in line')
            ngram = req_ngram(start_n=s_n, pos=tup, rhyme=rhyme, rhyme_not=rhyme_not, max_syllables=syllables, cur_syllables=syllable_count)
            end_rhyme = ngram['rhyme']
            print('\t\t>>>Ngram:', ngram)
            syllable_count += ngram['syllables']
        else:
            ngram = req_ngram(start_n=s_n, pos=tup, max_syllables=syllables,\
                cur_syllables=syllable_count)
            syllable_count += ngram['syllables']
            print('\t\t>>>Ngram:', get_string(ngram, skip_first=i>0))
        parts.append(get_string(ngram, skip_first=i>0))
    
    print('\n\nPARTS:', parts)
    return ' '.join(parts), end_rhyme


def gen_poem(form):
    stanzas = []
    lines = []
    s_n = 2
    # generate stanzas
    for stanza in form[1]:
        rhymes = {}
        # generate lines
        for line_num in range(stanza['lines']):
            print('--------------------------------------------------- Getting line', line_num + 1)
            # line requirements
            rym = stanza['rym'][line_num]
            pos = [p for p in stanza['pos'][line_num] if '<' not in p]
            print('\t', rym, pos)
            
            # query database
            r = None
            if rym in rhymes:
                r = rhymes[rym]
            line, new_r = gen_line(pos, syllables=stanza['syllables'][line_num],\
                rhyme=r, rhyme_not=list(rhymes.values()))
            rhymes[rym] = new_r
            print('\n\nLINE:', line)
            lines.append(line)            
            
        # print poem
        print('\n\nRhyme scheme:', stanza['rym'])
        print('\n\nStanza:')
        for l in lines:
            print('\t', l)
        stanzas.append(lines)
    return stanzas
            
            
            

if __name__ == '__main__':
    print('----------------------')
    print('CHOOSING FORM...')
    print('----------------------')
    folder = sys.argv[1]
    #form = fc.chooseForm(folder)
    #print(form)
    form = ([], [{'lines':6, 'rym':'aabbcc', 'syllables':[5,5,5,5,5,5], 'pos':[
        ['VB', 'AT', 'JJ', 'NN'],
        ['VB', 'AT', 'JJ', 'NN'],
        ['VB', 'AT', 'JJ', 'NN'],
        ['VB', 'AT', 'JJ', 'NN'],
        ['VB', 'AT', 'JJ', 'NN'],
        ['VB', 'AT', 'JJ', 'NN']
    ]}])
    print('\n----------------------')
    print('GENERATING CONTENT...')
    print('----------------------')
    poem = gen_poem(form)
            
            
            
            
            
            
            
            
