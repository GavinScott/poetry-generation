import praw
import os.path
import datetime
import time
import random

# returns a list of tuples like this: (post tile, post text)
# returns the top 'lim' posts from the given subreddit (lim < 1000)
def getPosts(subreddit, lim=1000):
    user_agent = "Poetry Generation Project: CSC 582 Cal Poly SLO"
    r = praw.Reddit(user_agent=user_agent)
    subs = []
    # set up timestamps
    dt = datetime.datetime(year=2016, month=11, day=13, hour=1).timetuple()
    e_t = time.mktime(dt)
    s_t = e_t
    DELTA = 86400 * 100
    # pull posts
    print 'Starting to pull posts...'
    li = ['something']
    while len(li) > 0:
        # build time range
        e_t = s_t
        s_t = e_t - DELTA
        timestamps = "timestamp:" + str(int(s_t)) + ".." + str(int(e_t))
        # handle submissions
        submissions = r.search(timestamps, subreddit=subreddit, limit=1000, syntax="cloudsearch")
        li = [(x.title.encode('utf-8').strip(), x.selftext.encode('utf-8').strip()) for x in submissions]
        # change delta if too many results
        if len(li) >= 999:
            print('\t\ttime range too large, cutting in half (min = 1 day)...')
            DELTA = int(DELTA / 2)
            DELTA = max(1, DELTA)
            s_t = e_t
        else:
            # add new results
            subs.extend(li)
            print '\tFound', len(subs), 'results...', '(' + str(len(li)), 'new)'
            # check if none returned
            if len(li) == 0:
                print 'Got all posts from', 'r/' + subreddit
                break
            elif len(li) < 600:
                print('\t\tgrowing delta...')
                DELTA = int(DELTA + (DELTA / 2))
        
    print 'Pulled', len(subs), 'posts'
    return subs

# give a directory ('haikus') and a list of poems. 'p' prints poems as it saves (if True)
# poems are either a list of lines, or a tuple with a title, then a list of lines
# poems with no provided title are numbered 'haiku-N'
def save(directory, poems, p=False):
    for poem in poems:
        if type(poem) != tuple:
            content = poem
            n = 1
            path = 'poems/' + directory + '/' + directory + '-' + str(n) + '.poem'
            while os.path.isfile(path):
                n += 1
                path = 'poems/' + directory + '/' + directory + '-' + str(n) + '.poem'
        else:
            path = 'poems/' + directory + '/' + poem[0].replace('/', '') + '.poem'
            if os.path.isfile(path):
                n = 1
                while os.path.isfile(path):
                    path = 'poems/' + directory + '/' + poem[0].replace('/', '') + '-' + str(n) + '.poem'
                    n += 1
            content = poem[1]
        if not os.path.isfile(path):
            try:
                with open(path, 'w+') as f:
                    for line in content:
                        f.write(str(line).strip() + '\n')  
                if p:
                    print '------------------------------------------------------'
                    print 'FILE:', path
                    print '\n'.join(content)
            except:
                print 'Weird I/O Error:', path
        else:
            print 'File already exists:', path

###############################################################################

def getHaikus(lim=1000):
    posts = getPosts('haiku', lim)
    poems = []
    for post in posts:
        # check valid posts
        if post[0].count('/') == 2:
            poem = post[0]
            if '::' in poem:
                poem = post[0].split('::')[1]
            poems.append([x.strip() for x in poem.split('/')])
    print 'Got', len(poems), 'haikus!'
    return poems
    
def getLimericks(lim=1000):
    posts = getPosts('limericks', lim)
    poems = []
    for post in posts:
        title = post[0]
        content = [x.strip() for x in post[1].split('\n') if len(x.strip()) > 0]
        # check valid
        if len(content) == 5:
            poems.append((title, content))
    print 'Got', len(poems), 'limericks!'
    return poems

def getFreeform(lim=1000):
    posts = getPosts('OCpoetry', lim)
    poems = []
    for post in posts:
        title = post[0]
        content = [x.strip() for x in post[1].split('\n') if len(x.strip()) > 0 \
            and 'www.' not in x and '.com' not in x]
        poems.append(content)
    # save time later
    random.shuffle(poems)
    return poems[:100]

if __name__ == '__main__':
    # getting haikus
    #haikus = getHaikus()
    #save('haikus', haikus)
    
    # getting limericks
    #limericks = getLimericks()
    #save('Limericks', limericks)
    
    # get free-form
    freeform = getFreeform(200)
    save('freeform', freeform)
    
