import sys
import random
import os
import features
import builder2
import nltk

NO_NEW_DATA_FLAG = '-no_new'

# loads a single json file into a format for training
def load_single_json_features(directory, fname=None, json=None):
    try:
        if json == None:
            json = features.loadFeatures(fname)
        feats = json[0]
        # combine local and global features
        keys = ('lines', 'rym', 'syllables', 'rep', 'pos') # could add 'pos'
        for key in keys:
            # only get local features from the first stanza
            if type(json[1][0][key]) == list:
                json[1][0][key] = tuple(json[1][0][key])
            if key == 'pos':
                feats[key] = tuple([tuple(pos) for pos in json[1][0][key]])
            else:
                feats[key] = json[1][0][key]
        return (feats, directory)
    except:
        return None

# takes a list of json file names and returns a list of poem features
def load_features_from_json(directory, files):
    feats = []
    for fname in files:
        f = load_single_json_features(directory, fname=fname)
        if f != None:
            feats.append(f)
    return feats

# taks a list of directories and returns training data
def build_training_data(poem_directories):
    # get all feature files from each directory
    files = {}
    #poem_directories = ['poems/' + d for d in poem_directories]
    for directory in poem_directories:
        files[directory] = ['poems/' + directory + fname for fname in \
            os.listdir('poems/' + directory) if '.json' in fname]
    # ensure each poem type has same representation in data
    length_cap = min([len(files[directory]) for directory in poem_directories])
    # randomize & trim data, then add to returned list
    data = []
    for directory in poem_directories:
        random.shuffle(files[directory])
        files[directory] = load_features_from_json(directory, files[directory])
        data.extend(files[directory][:length_cap])
    # shuffle final data and return
    random.shuffle(data)
    print('Training data:', len(data), 'poems')
    return data
    
# returns 'num_poems' new poems of each type, shuffles and formatted
# for easy classification ({features}, "class")
def generate_new_data(poem_directories, db_name, num_poems):
    data = []
    cur_num = 0
    for directory in poem_directories:
        dir_data = []
        if len(os.listdir(builder2.CACHE_DIR + directory)) == 0:
            cur_num = 0
        else:
            cur_num = max([int(x.replace('.json', '').replace('.poem', '')) \
                for x in os.listdir(builder2.CACHE_DIR + directory)]) + 1
            for i in range(cur_num):
                new_fname = builder2.CACHE_DIR + directory + '/' + str(i) + '.poem'
                poem_feats = features.extract(new_fname)
                #os.remove(new_fname)
                # reformat for classification
                poem_features = load_single_json_features(directory, json=poem_feats)
                dir_data.append(poem_features)
        while len(dir_data) < num_poems:
            # generate poem
            poem = builder2.build_poem_from_cache_and_db(directory, db_name)
            # save poem to a file
            new_fname = builder2.CACHE_DIR + directory + str(cur_num) + '.poem'
            cur_num += 1
            with open(new_fname, 'w+') as f:
                for i in range(len(poem)):
                    stanza = poem[i]
                    for line in stanza:
                        print('LINE:', line)
                        f.write(line + '\n')
                    if i < len(poem) - 1:
                        f.write('\n')
            # extract features
            poem_feats = features.extract(new_fname)
            #os.remove(new_fname)
            # reformat for classification
            poem_features = load_single_json_features(directory, json=poem_feats)
            dir_data.append(poem_features)
        print('LOADED', len(dir_data), directory, 'POEMS')
        data.extend(dir_data)
            
    # shuffle and return the data
    random.shuffle(data)
    print('\n\nLOADED DATA FROM', len(data), 'TOTAL POEMS')
    return data

class FormClassifier():
    def __init__(self, dirs):
        all_dirs = dirs[:]
        if '-freeform' in sys.argv:
            all_dirs.append('freeform/')
        self.train = build_training_data(all_dirs)
        self.classifier = nltk.NaiveBayesClassifier.train(self.train)
        self.dirs = dirs
        self.new_data = None
        self.test_baseline()
        
    def multiple_baseline_tests(self, n):
        accs = []
        for i in range(n):
            self.train = build_training_data(self.dirs)
            accs.append(self.test_baseline())
        avg = sum(accs) / len(accs)
        print('\n\nAccuracy over', n, 'trials:', avg)
        return avg            
    
    # generates and saves new poems for evaluation
    def generate_data(self, db, num_poems):
        self.new_data = generate_new_data(self.dirs, db, num_poems)
        return self.new_data
    
    # tests accuracy on the training data (train with 2/3, test on 1/3)
    def test_baseline(self):
        nb = nltk.NaiveBayesClassifier.train(self.train[int(len(self.train) / 3):])
        acc = nltk.classify.accuracy(nb, self.train[:int(len(self.train) / 3)])
        print('\tAccuracy on Training Data:', acc)
        return acc
    
    # tests accuracy on either the given set or the data generated
    # from 'generate_data'
    def test_generated(self, data=None):
        if self.new_data == None and data == None:
            print('Error: No generated data. Run "generate_data"')
            exit(-1)
        if data != None:
            # use provided data
            acc = nltk.classify.accuracy(self.classifier, data)
        else:
            # use previously generated data
            acc = nltk.classify.accuracy(self.classifier, self.new_data)
        print('\tAccuracy on Generated Data:', acc)
        return acc
        

if __name__ == '__main__':
    if len(sys.argv) < 3 or '-h' in sys.argv or '.db' not in sys.argv[1]:
        print('Usage: python3 eval.py content_db num_poems [poem_dir1, poem_dir2, ...] [-no_new] [-fullrun]')
        exit(-1)
    db = sys.argv[1]
    num_poems = int(sys.argv[2])
    dirs = [d for d in sys.argv[3:] if '-' not in d]
    print('\nTesting on these directories:', dirs)
    print('Will generate', num_poems, 'of poems of each type with:', db)
    
    form_classifier = FormClassifier(dirs)
    if '-fullrun' in sys.argv:
        avg = form_classifier.multiple_baseline_tests(100)
        
    if NO_NEW_DATA_FLAG not in sys.argv:
        form_classifier.generate_data(db, num_poems)
        form_classifier.test_generated()
    
    
    
    
    
    
