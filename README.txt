EloquentRobot: Automatic Poetry Generation
Gavin Scott
Jeff McGovern

CPE 582-01
Fall 2016

Automatic content generation is a growing field with applications in creative writing and video-games.  One aspect of lore and creative content is poetry, defined by features that may include, rhyme, repeated lines, word stress, meter, and parts of speech.  Generating believable poetry can aid writers add more believability to their stories.  To aid the task of content generation we built EloquentRobot, a Python-based tool that mines existing poems for structure and a corpus for content and procedurally generates poetry using the extracted features.   By  building a classifier for poetry structure, EloquentRobotwas able to obtain a high accuracy for the classification of our generated poetry.

Generating Poetry
-----------------
Requirements:
	--NLTK
	--sqlite3
Instructions:
Extract both the source and data tarballs into the same directory.
Your directory should look like this:

	[jmcgover@MAINFRAME poetry-generation]$ ls -lh
	total 414M
	drwxr-xr-x 3 jmcgover jmcgover 4.0K Dec 16 22:51 data
	-rw-r--r-- 1 jmcgover jmcgover 412M Dec 16 23:15 EloquentRobot_data.tar.gz
	-rw-r--r-- 1 jmcgover jmcgover 228K Dec 16 22:55 EloquentRobot.pdf
	-rw-r--r-- 1 jmcgover jmcgover 252K Dec 16 21:53 EloquentRobot_presentation.pdf
	-rw-r--r-- 1 jmcgover jmcgover 1.6M Dec 16 23:12 EloquentRobot_source.tar.gz
	-rw-r--r-- 1 jmcgover jmcgover  47K Dec 16 22:56 EloquentRobot.zip
	-rw-r--r-- 1 jmcgover jmcgover 1.7K Dec 16 23:18 README.txt
	drwxr-xr-x 7 jmcgover jmcgover 4.0K Dec 16 22:51 src

Move into the src directory:
	cd src/

Run the builder by providing either a folder of poetry or a template
	python builder2.py poems/Limericks/ ../data/brown.db
	python builder2.py poems/haiku/ ../data/trump.db
	python builder2.py limerick ../data/insult.db
	python builder2.py haiku ../data/curses.db

Note:
	--Ensure that you include the '/' in the directory path
	--If a poem doesn't seem like it's the right format when generated from the provided folder, inspect the message printed above the poem to see the syllables layout statistically generated from the folder
	--The data/ folder is primarily data that we have formatted, but additional scraped data is included in the src/poems folder
