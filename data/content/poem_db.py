import sqlite3
import content_parser as cp

def connect(name):
    conn = sqlite3.connect(name)
    print("Opened database successfully")
    return conn

def pop_words(name, ngrams):
    conn = connect(name)
    # create table
    print('\nCreating table...')
    try:
        conn.execute('''
            CREATE TABLE poems (
                f VARCHAR(50),
                s VARCHAR(50),
                m VARCHAR(200),
                sl VARCHAR(50),
                l VARCHAR(50),
                pos_f VARCHAR(20),
                pos_s VARCHAR(20),
                pos_sl VARCHAR(20),
                pos_l VARCHAR(20),
                pos_m VARCHAR(20),
                rhy VARCHAR(25),
                n INT,
                PRIMARY KEY (f, s, m, sl, l, pos_f, pos_s, pos_sl, pos_l, pos_m, rhy, n)
            );''')
    except:
        print(name, 'already exists, skipping creation...')
    # populate
    print('\nPopulating...')
    dups = 0
    inserts = 0
    for f in ngrams:
        try :
            cols = ','.join([feat for feat in cp.FEAT_LIST])
            vals = ','.join(['"' + str(f[feat]) + '"' for feat in cp.FEAT_LIST])
            cmd = 'INSERT INTO poems (' + cols + ') VALUES (' + vals + ')'
            conn.execute(cmd)
            inserts += 1
        except:
            dups += 1
    print('\n' + str(inserts), 'inserts attempted')
    if dups > 0:
        print('NOTE:', dups, 'inserts failed (probably duplicate entries in table)')
    else:
        print('\tAll inserts successful')
    # finish
    print('\nCommitting changes...')
    conn.commit()
    print('Done, closing connection')
    conn.close()

def query_all(name):
    print('\nQuerying All...')
    conn = connect(name)
    q = 'SELECT * FROM poems'
    cursor = conn.execute(q)
    for row in cursor:
       print(row)
    print('\nDone, closing connection')
    conn.close()
    
