from nltk.corpus import brown
from nltk.corpus import cmudict
import copy
import poem_db as db
import sys

FIRST = 'f'
LAST = 'l'
SECOND = 's'
SECOND_LAST = 'sl'
MIDDLE = 'm'
POS_FIRST = 'pos_f'
POS_LAST = 'pos_l'
POS_SECOND = 'pos_s'
POS_SECOND_LAST = 'pos_sl'
POS_MIDDLE = 'pos_m'
N = 'n'
RHYME = 'rhy'
SYLL = 'syllables'

FEAT_LIST = [FIRST, LAST, SECOND, SECOND_LAST, MIDDLE, N, RHYME,
    POS_FIRST, POS_LAST, POS_SECOND, POS_SECOND_LAST, POS_MIDDLE]

def get_ngrams(words, n):
    grams = []
    for i in range(0, len(words) - n + 1):
        gram = []
        for x in range(n):
            gram.append(words[i + x])
        grams.append(gram)
    return grams

def get_all_ngrams(words, n_start, n_end):
    ngrams = {}
    for n in range(n_start, n_end + 1):
        ngrams[n] = get_ngrams(words, n)
        print('done getting ngrams for n =', n)
    return ngrams

def get_partial_ngram_features(ngrams):
    all_features = []
    all_lasts = [gram[-1][0] for gram in ngrams]
    for gram in ngrams:
        feats = {}
        feats[N] = len(gram)
        #feats[POS] = '|'.join([g[1] for g in gram])
        feats[FIRST] = gram[0][0]
        feats[POS_FIRST] = gram[0][1]
        feats[LAST] = gram[-1][0]
        feats[POS_LAST] = gram[-1][1]
        if len(gram) > 2:
            feats[SECOND] = gram[1][0]
            feats[POS_SECOND] = gram[1][1]
            feats[SECOND_LAST] = gram[-2][0]
            feats[POS_SECOND_LAST] = gram[-2][1]
        else:
            feats[SECOND] = 'NULL'
            feats[POS_SECOND] = 'NULL'
            feats[SECOND_LAST] = 'NULL'
            feats[POS_SECOND_LAST] = 'NULL'
        if len(gram) > 4:
            feats[MIDDLE] = '|'.join([g[0] for g in gram[2:-2]])
            feats[POS_MIDDLE] = '|'.join([g[1] for g in gram[2:-2]])
        else:
            feats[MIDDLE] = 'NULL'
            feats[POS_MIDDLE] = 'NULL'
        all_features.append(feats)
    return all_features

def get_full_ngram_features(ngrams, db_name=None):
    print('Generating features for', len(ngrams), 'ngrams...')
    fs = get_partial_ngram_features(ngrams)
    print('\tDone with partial features...')
    final_features = []
    # truncate cmudict
    lasts = set([gram[-1][0] for gram in ngrams])
    print('\tTruncating CMUdict...')
    d = [(w, pron) for (w, pron) in cmudict.entries() if w in lasts]
    print('\t\tDone')
    print('\tDoing rhyming features...')
    # add rhyme features
    rhymes = {}
    for f in fs:
        if f[LAST] not in rhymes:
            rhymes[f[LAST]] = getRhyme(f[LAST], d)
        for r in rhymes[f[LAST]]:
            f[RHYME] = r
            final_features.append(copy.deepcopy(f))
            if len(final_features) % 500000 == 0:
                print('\t\tDone with', len(final_features), 'ngrams...')
                if db_name != None:
                    print('\t\t\tPutting in database and clearing RAM (somebody needs to upgrade...)')
                    db.pop_words(db_name, final_features)
                    print('\t\t\tContinuing to get features...')
                    final_features = []
    return final_features

def getRhyme(word, d=cmudict.entries()):
    # get last word pronunciations
    p = [pron[-(min(3, int(len(pron) / 2))):] for (w, pron) in d if w == word]
    p = ['|'.join(x) for x in p]
    p = set([''.join([c for c in s if not c.isdigit()]) for s in p])
    return p

def get_all_ngram_features(ngrams, db_name=None):
    all_f = []
    for n in ngrams:
        for f in ngrams[n]:
            all_f.append(f)
    feats = get_full_ngram_features(all_f, db_name)
    print('done getting features for n =', n)
    return feats

def get_brown_tags():
    words = []
    if '-test' in sys.argv:
        for word, pos in brown.tagged_words()[:20]:
            words.append((word.lower(), pos))
    else:
        for word, pos in brown.tagged_words():
            words.append((word.lower(), pos))    
    return words

def do_corpus(words, n_start, n_end, db_name=None):
    ngrams = get_all_ngrams(words, n_start, n_end)
    return get_all_ngram_features(ngrams, db_name)
    
def do_brown_corpus(n_start, n_end, db_name=None):
    return do_corpus(get_brown_tags(), n_start, n_end, db_name)
    

