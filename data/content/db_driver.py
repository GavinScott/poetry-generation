import poem_db as db
import content_parser as cp
import sys

DB = 'Poems.db'

SHITTY_RAM = '-noram' # command line flag to admit that you need more RAM

if SHITTY_RAM in sys.argv:
    # slightly slower, ram saving option
    print('Populating database with RAM-saving option...')
    brown = cp.do_brown_corpus(2, 2, DB)
else:
    # does it all at once
    print('Populating database...')
    brown = cp.do_brown_corpus(2, 2)
    db.pop_words(DB, brown)

# query for proof
#db.query_all(DB)
